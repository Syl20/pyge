#!/usr/bin/env python3.6
# -*-coding:Utf-8 -*

"""Initialization for pyge program. Check if configuration file is OK"""

#_Import_module_________________________________________________________________

import sys
import configparser

#_General_variable______________________________________________________________

config_file_path = "pyge.conf" # Configuration file path

configuration = configparser.ConfigParser(interpolation=configparser.BasicInterpolation()) # Configuration object
try: # Open file for parsing configuration file
    open(config_file_path)
    configuration.read(config_file_path) # Try to read configuration file

    path_log = configuration["Log"]["path"]
    time_log = configuration["Log"]["time"]

    path_record = configuration["Record"]["path"]
    duration_record = float(configuration["Record"]["duration"])

    channels_list = configuration["Channels"]["channels_list"].split(',')
    channels_inputs = {}
    for channel in channels_list:
        channels_inputs[channel] = configuration["Channels"][channel]

    encoding_bitrate = configuration["Encoding"]["bitrate"]
    encoding_frame_size = configuration["Encoding"]["frame_size"]
    encoding_gop = configuration["Encoding"]["gop"]
    encoding_segment_time = configuration["Encoding"]["segment_time"]
    encoding_segment_name = configuration["Encoding"]["segment_name"]
    encoding_preset = configuration["Encoding"]["preset"]

except IOError as error_file : # Quit if cannot find configuration file
    print("Exit 2 : Error on configuration file > " + str(error_file))
    sys.exit(2)
except KeyError as error_key :
    print("Exit 1 : No input for {} in configuration file {}".format(str(error_key),config_file_path))
    sys.exit(1)
except Exception as error :
    print("Exit 1 : Error in configuration file > " + str(error))
    sys.exit(1)

if __name__ == '__main__':

    print(" \
    path_log : {} \n \
    time_log : {} \n \
    path_record : {} \n \
    duration_record : {} \n \
    channels_list : {} \n \
    channels_inputs : {} \n \
    encoding_frame_size : {} \n \
    encoding_bitrate : {} \n \
    encoding_preset : {} \n \
    encoding_gop : {} \n \
    encoding_segment_time : {} \n \
    encoding_segment_name : {} \n" \
    .format(path_log,time_log,path_record,duration_record,channels_list,\
    channels_inputs,encoding_frame_size,encoding_bitrate,encoding_preset,\
    encoding_gop,encoding_segment_time,encoding_segment_name))

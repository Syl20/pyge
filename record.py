#!/usr/bin/env python3.6
# -*-coding:Utf-8 -*

"""Start the recorder for each channel"""

#_Import_module_________________________________________________________________

import time
import signal
import ffmpy

from configuration import path_log, path_record,channels_list,\
channels_inputs,encoding_frame_size,encoding_bitrate,encoding_preset,\
encoding_gop,encoding_segment_time,encoding_segment_name
import function

def record(channel, ffmpeg_process, initialization=1):

    channel_input = channels_inputs[channel] # Get the input.
    ffmpeg_log_time = time.strftime("_%Y%m%d-%H%M%S")
    ffmpeg_log_file = path_log + channel + ffmpeg_log_time + "_ffmpeg.log"
    ffmpeg_log=open(ffmpeg_log_file,"a") # Open file to log ffmpeg stderr.

    # Configure ffmpeg command.
    ffmpeg = ffmpy.FFmpeg(
    inputs = {channel_input : '-ac 2 '},
    outputs = {path_record + channel + "/" + channel + encoding_segment_name : \
                '-map 0 ' \
                '-s:v ' + encoding_frame_size + \
                ' -vf ' "drawtext=fontfile=Verdana.ttf:text='%{localtime\}':fontsize=32:fontcolor=yellow@1:x=7:y=7 " \
                ' -vcodec libx264 -b:v ' + encoding_bitrate + ' -g ' + encoding_gop + ' -preset ' + encoding_preset + \
                ' -acodec copy' \
                ' -scodec copy' \
                ' -f segment -segment_atclocktime 1 -reset_timestamps 1 -segment_time ' + encoding_segment_time + \
                ' -use_localtime 1 -strftime 1 ' \
                '-benchmark -y '}
    )
    ffmpeg.run(stderr = ffmpeg_log) # Run ffmpeg subprocess, redirect stderr to ffmpeg_log.

    if initialization: # Different implementation if first launch of ffmpeg or relaunch
        function.log("FFMPEG",ffmpeg.cmd) # Log ffmpeg command.
        print("Start {} ... ".format(channel),end='',flush=True)
        time.sleep(5) # Wait for process to initialize.
        if ffmpeg.process.poll() is not None: # Check if ffmpeg start correctly.
            function.log("ERROR FFMPEG","Cannot start ffmpeg. Please check " + ffmpeg_log_file )
            function.exit_pyge(signal.SIGINT,function.exit_pyge,3)
        else:
            function.log("FFMPEG","START OF RECORD : {} at {} with PID {}. Log to {}" .format(channel,channel_input,str(ffmpeg.process.pid),ffmpeg_log_file))
            print("OK")
            ffmpeg_process[channel] = ffmpeg # Store actual ffmpeg object with channel key.
    else:
        function.log("FFMPEG","START OF RECORD : {} at {} with PID {}. Log to {}" .format(channel,channel_input,str(ffmpeg.process.pid),ffmpeg_log_file))
        ffmpeg_process[channel] = ffmpeg # Store actual ffmpeg object with channel key.

    return ffmpeg_process

if __name__ == '__main__':

    import subprocess

    ffmpeg_process = {}
    for channel in channels_list:
        record(channel, ffmpeg_process)

    function.exit_pyge(signal.SIGTERM,function.exit_pyge,0)

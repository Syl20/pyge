#!/usr/bin/env python3.6
# -*-coding:Utf-8 -*

#_Import_module_________________________________________________________________
import os
import time
import signal

import configuration
import function
import record

# To catch SIGTERM signal and run exit function
signal.signal(signal.SIGTERM, function.exit_pyge)
signal.signal(signal.SIGINT, function.exit_pyge)

function.log("RUN", "___________________START_OF_PROGRAM___________________")

# Check directory
function.check_directory(configuration.path_log)
function.check_directory(configuration.path_record)

# Start of record
ffmpeg_process = {} # Store ffmpeg objects
for channel in configuration.channels_list:
    function.check_directory(configuration.path_record + channel)
    ffmpeg_process = record.record(channel,ffmpeg_process)

while True:
    for channel in configuration.channels_list:

        # Relaunch ffmpeg process if stoped
        if ffmpeg_process[channel].process.poll() is not None:
            function.log("FFMPEG ERROR","Relaunch record for " + channel)
            record.record(channel,ffmpeg_process,0) # Relaunch ffmpeg so initialization is 0

        # List files and erase file older than duration_record
        try:
            for record_file in os.listdir(configuration.path_record + channel):

                record_file_path = configuration.path_record + channel + '/' + record_file # Full path for record files
                time_file = os.stat(record_file_path).st_mtime
                time_now = time.time()

                if time_now - configuration.duration_record >= time_file:
                    os.remove(record_file_path)
                    function.log("FILE", "{} older than {} seconds was removed.".format(record_file,configuration.duration_record))

        except Exception as error :
            function.log("FILE", "Cannot access to storage : " + str(error))
            time.sleep(30)

    time.sleep(10)

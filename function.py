#!/usr/bin/env python3.6
# -*-coding:Utf-8 -*

import os
import sys
import signal
import configuration
import time


def log(log_level, log_details, log_path=configuration.path_log):
    """Format, add timestamp and write log_level and
        log_details in the log file define by log_path."""

    try:
        with open(log_path + "run.log", "a") as log_file:
            log_file.write(time.strftime(configuration.time_log) + " > " + "[" + log_level + "]" + " > " + log_details + "\n")
    except Exception as error_log:
        sys.exit('Exit 2 : Error on log file > ' + str(error_log))

def check_directory(path):
    """Check if directory define by path is created, if not create directory."""

    try:
        if os.access(path,os.W_OK) and os.access(path,os.R_OK): # Check rights on directory.
            log("FILE", "Checks OK for " + path)
        else : # Path don't exist or has no R/W rights.
            os.makedirs(path)
            log("FILE", path + " : Created.")
    except Exception as error_file :
        log("FILE ERROR", "Checks not OK for {} : {}".format(str(path),str(error_file)))
        exit_pyge(signal.SIGINT,exit_pyge,2)

def exit_pyge(signal,frame,exit_code=1):
    """Print message and exit program."""

    if exit_code == 0:
        print("Exit " + str(exit_code))
    else:
        print("Exit " + str(exit_code) + ". Please check logs : " + configuration.path_log)
    sys.exit(exit_code)

if __name__ == '__main__' :

# Test every function of this module

    print("Test of function.log() ...", end='', flush=True)
    log("TEST",'This is a log test written by init.py')
    print("OK")

    print("Test of function.check_directory() ...", end='', flush=True)
    check_directory(configuration.path_log)
    check_directory(configuration.path_record)
    for channel in configuration.channels_list:
        check_directory(configuration.path_record + channel)
    print("OK")

    print("Test of function.exit_pyge() ...", end='', flush=True)
    import signal
    exit_pyge(signal.SIGTERM,exit_pyge,0)

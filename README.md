# Pyge
_A simple TV (or any live source) logging program writing in Python3 and using FFmpeg_

### Requirements
- \*NIX operating system  
- Python3  
- FFmpeg  

### Installation
- Install ffmpy package and comment some lines (*see ffmpy section*)  
> pip install ffmpy  
> pip show ffmpy  
> nano ffmpy.py
- Clone repo  
> git clone https://framagit.org/Syl20/pyge.git
- Edit pyge.conf  
> nano pyge.conf  
- Run pyge.py  
> ./pyge.py  


### ffmpy
_I will try to natively integrate ffmpy code in pyge soon, to make things easier..._

For now you must install ffmpy package (https://github.com/Ch00k/ffmpy ,thanks to him) and comment these lines in **ffmpy.py run function definition** :

*103         # out = self.process.communicate(input=input_data)  
104         # if self.process.returncode != 0:  
105         #    raise FFRuntimeError(self.cmd, self.process.returncode, out[0], out[1])  
106  
107         # return out*
